#
#  Be sure to run `pod spec lint QYMKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "QYMKit"
  spec.version      = "0.0.21"
  spec.summary      = "项目常用文件使用的库"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = "常用类别、基础方法的集合"

  spec.homepage       = "https://gitee.com/lz156/QYMKit"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  spec.license      = "MIT"
  # spec.license      = { :type => "MIT", :file => "FILE_LICENSE" }


  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the authors of the library, with email addresses. Email addresses
  #  of the authors are extracted from the SCM log. E.g. $ git log. CocoaPods also
  #  accepts just a name if you'd rather not provide an email address.
  #
  #  Specify a social_media_url where others can refer to, for example a twitter
  #  profile URL.
  #

  spec.author             = { "zhen" => "" }
  # Or just: spec.author    = "zhen"
  # spec.authors            = { "zhen" => "" }
  # spec.social_media_url   = "https://twitter.com/zhen"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If this Pod runs only on iOS or OS X, then specify the platform and
  #  the deployment target. You can optionally include the target after the platform.
  #

  # spec.platform     = :ios
  # spec.platform     = :ios, "5.0"

  #  When using multiple platforms
  spec.ios.deployment_target = "8.0"
  # spec.osx.deployment_target = "10.7"
  # spec.watchos.deployment_target = "2.0"
  # spec.tvos.deployment_target = "9.0"


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Specify the location from where the source should be retrieved.
  #  Supports git, hg, bzr, svn and HTTP.
  #

  spec.source       = { :git => "git@gitee.com:lz156/QYMKit.git", :tag => "v#{spec.version}" }


  # ――― Source Code ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  CocoaPods is smart about how it includes source code. For source files
  #  giving a folder will include any swift, h, m, mm, c & cpp files.
  #  For header files it will include any header in the folder.
  #  Not including the public_header_files will make all headers public.
  #

  spec.source_files  = "QYMKit/*.h"
  #spec.exclude_files = "Classes/Exclude"

  spec.public_header_files = "QYMKit/*.h"


  # ――― Resources ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  A list of resources included with the Pod. These are copied into the
  #  target bundle with a build phase script. Anything else will be cleaned.
  #  You can preserve files from being cleaned, please don't preserve
  #  non-essential files like tests, examples and documentation.
  #

  # spec.resource  = "icon.png"
  # spec.resources = "Resources/*.png"

  # spec.preserve_paths = "FilesToSave", "MoreFilesToSave"


  # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Link your library with frameworks, or libraries. Libraries do not include
  #  the lib prefix of their name.
  #

  # spec.framework  = "SomeFramework"
  # spec.frameworks = "SomeFramework", "AnotherFramework"

  # spec.library   = "iconv"
  # spec.libraries = "iconv", "xml2"


  # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  If your library depends on compiler flags you can set them in the xcconfig hash
  #  where they will only apply to your library. If you depend on other Podspecs
  #  you can include multiple dependencies to ensure it works.

  spec.requires_arc = true

  # spec.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # spec.dependency "JSONKit", "~> 1.4"

  spec.subspec "Categories" do |categoriesspec|

  #categoriesspec.public_header_files = "QYMKit/Categories/**/*.h"
  #categoriesspec.source_files        = "QYMKit/Categories/**/*.{h,m}"

    categoriesspec.subspec "UIColor" do |colorSubSpec|

    colorSubSpec.source_files = "QYMKit/Categories/UIColor/*.{h,m}"

    end

    categoriesspec.subspec "UIImage" do |imageSubSpec|

    imageSubSpec.source_files = "QYMKit/Categories/UIImage/*.{h,m}"

    end

  end

  spec.subspec "Tools" do |toolspec|

    #categoriesspec.public_header_files = "QYMKit/Categories/**/*.h"
    #categoriesspec.source_files        = "QYMKit/Categories/**/*.{h,m}"

    toolspec.subspec "NetworkAuthority" do |networkAuthoritySubSpec|

    networkAuthoritySubSpec.ios.framework = 'Foundation','UIKit','CoreTelephony','SystemConfiguration'
    networkAuthoritySubSpec.source_files  = "QYMKit/Tools/NetworkAuthorityManage/*.{h,m}"

    end
    
    toolspec.subspec "Location" do |locationSpec|
      
      locationSpec.ios.framework = 'Foundation','UIKit','CoreLocation'
      locationSpec.source_files = "QYMKit/Tools/Location/*.{h,m}"
      
    end
    
    toolspec.subspec "AES128Helper" do |aes128HelperSpec|
      
      aes128HelperSpec.source_files         = "QYMKit/Tools/AES/framework/*.{h,m}","QYMKit/Tools/AES/Classes/*.{h,m}"
      aes128HelperSpec.ios.vendored_library = "QYMKit/Tools/AES/framework/libsecret.a"
      
    end
    
    toolspec.subspec "SafeKeyboard" do |safeKeyboardSpec|
      
      safeKeyboardSpec.source_files = "QYMKit/Tools/SafeKeyboard/JLPwdTextField/*.{h,m}",
        "QYMKit/Tools/SafeKeyboard/JLSecurityTextField/*.{h,m}",
        "QYMKit/Tools/SafeKeyboard/Keyboard/*.{h,m}",
        "QYMKit/Tools/SafeKeyboard/Keyboard/KeyBoardButton/*.{h,m}",
        "QYMKit/Tools/SafeKeyboard/Keyboard/KeyboardView/*.{h,m}",
        "QYMKit/Tools/SafeKeyboard/Category/*.{h,m}"
      safeKeyboardSpec.resources = "QYMKit/Tools/SafeKeyboard/Keyboard/NumKeyboard.bundle"
      safeKeyboardSpec.dependency 'Masonry'
      
    end
    
    toolspec.subspec "DeviceUUID" do |deviceUUIDSpec|
      deviceUUIDSpec.source_files = "QYMKit/Tools/DeviceUUID/*.{h,m}"
      deviceUUIDSpec.dependency 'KeychainItemWrapper'
    end
    
    toolspec.subspec "AppInfo" do |appInfoSpec|
      appInfoSpec.source_files = "QYMKit/Tools/AppInfo/*.{h,m}"
    end
  
  end
  
  
  


end

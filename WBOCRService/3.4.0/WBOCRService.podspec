
Pod::Spec.new do |s|
  s.name             = 'WBOCRService'
  s.version          = '3.4.0'    
  s.summary          = 'A short description of WBHJOCRService.'
  s.description      = "ocr sdk."
  s.homepage         = 'http://git.webank.io/tankxie/WBOCRServicePod'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'tankxie@webank.com' => 'tankxie@webank.com' }
  #s.source           = { :path => '.' }
  s.source  =  { :git => "https://gitlab.jlpay.com/access-services/common-components/ios-component.git", :tag => "#{s.name}_v#{s.version}" }
  
  s.ios.deployment_target = '8.0'
  s.library = 'c++'
  s.frameworks = "CoreTelephony", "CoreServices", "CoreMedia", "AssetsLibrary","AVFoundation","WebKit","SystemConfiguration"
  
  s.resource_bundles = {
    'WBOCRService' => ['WBOCRService/Libs/WBOCRService.bundle/*.png']
  }
  
  s.subspec 'BinaryLib' do |lib|
    lib.vendored_libraries = "WBOCRService/Libs/*.a"
    lib.vendored_frameworks = "WBOCRService/Libs/*.framework"
  end
  
 
end

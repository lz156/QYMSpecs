#
# Be sure to run `pod lib lint QYMImagePicker.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'QYMImagePicker'
  s.version          = '0.1.0'
  s.summary          = 'ios image download and cache.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = '调用系统拍照已经相册权限处理（ImagePicker image）'

  s.homepage         = 'https://github.com/lz156/QYMSpecs'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = 'MIT'
  s.author           = { 'lz156' => 'lz156@foxmail.com' }
  s.source           = { :git => "https://git.oschina.net/lz156/QYMImagePicker.git", :tag => s.version.to_s}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '7.0'

  s.source_files = 'QYMImagePicker/Classes/**/*'
  
  # s.resource_bundles = {
  #   'QYMImagePicker' => ['QYMImagePicker/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
